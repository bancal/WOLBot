# WOLBot - a telegram bot to Wake-On-Lan your machines


## SETUP

```bash
pipenv --three install
```

Prepare `secret.json` file following `secret_example.json` syntax!


## RUN it

```bash
pipenv run python bot.py
```
