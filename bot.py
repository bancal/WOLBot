import json
import logging
import datetime
import subprocess
from telegram.ext import Updater, CommandHandler

SECRET_JSON = './secret.json'
WOL_BIN = '/usr/bin/wakeonlan'
PING_BIN = '/bin/ping'
CHECK_INTERVAL = 1  # seconds
MINUTES_TO_WAIT_FOR_PING = 2  # max nb minutes to wait for a WOLed host to answer to ping

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)


def send_wol(mac_address):
    cmd = [WOL_BIN, mac_address]
    subprocess.call(cmd)


def check_user_allowed(bot, update):
    if update.message.from_user.username not in secret['users']:
        update.message.reply_text('''\
Unknown user. You have to be granted access to this bot. Sorry.''')
        return False
    return True


def start_command(bot, update):
    if check_user_allowed(bot, update):
        update.message.reply_text(
            '''\
Hello {}

This bot is here to send Wake On Lan packets to preconfigured hosts.
You'll receive a notification when the host is up.

/help - List available commands.'''.format(update.message.from_user.first_name))


def help_command(bot, update):
    if check_user_allowed(bot, update):
        update.message.reply_text('''\
This bot is here to send Wake On Lan packets to preconfigured hosts.
You'll receive a notification when the host is up.

/wol name - Send a Wake On Lan packet to the desired host.

/list - List the names and hostnames available for you.''')


def list_command(bot, update):
    if check_user_allowed(bot, update):
        username = update.message.from_user.username
        answer = 'Here are the available WakeOnLan commands for you\n\n'
        for name, host in secret['users'][username].items():
            answer += '/wol %s - to wake up %s (%s)\n' % (
                name, host['hostname'], host['mac'])
        update.message.reply_text(answer)


def wol_command(bot, update, args, job_queue):
    if check_user_allowed(bot, update):
        if len(args) == 1:
            chat_id = update.message.chat_id
            username = update.message.from_user.username
            name = args[0]
            try:
                host = secret['users'][username][name]
                send_wol(host['mac'])
                job = job_queue.run_once(
                    ping_host,
                    CHECK_INTERVAL,
                    context={
                        'chat_id': chat_id,
                        'forget_it_datetime': datetime.datetime.now() + datetime.timedelta(minutes=MINUTES_TO_WAIT_FOR_PING),
                        'hostname': host['hostname'],
                        'job_queue': job_queue,
                    }
                )
                update.message.reply_text(
                    '''\
WakeOnLan packet was sent to %s
We'll let you know within %d minutes when %s is up.''' % (host['mac'], MINUTES_TO_WAIT_FOR_PING, host['hostname']))
            except KeyError:
                update.message.reply_text('unknown host %s' % name)
        else:
            update.message.reply_text('this command requires one argument.')

def ping_host(bot, job):
    logger.info('ping_host')
    cmd = [PING_BIN, '-c1', '-w1', job.context['hostname']]
    return_code = subprocess.call(cmd)
    if return_code == 0:
        bot.send_message(
            job.context['chat_id'],
            text='%s is up now!' % job.context['hostname'])
    else:
        if datetime.datetime.now() > job.context['forget_it_datetime']:
            bot.send_message(
                job.context['chat_id'],
                text='%s could not be awake : (' % job.context['hostname'])
        else:
            job.context['job_queue'].run_once(
                ping_host,
                CHECK_INTERVAL,
                context=job.context
            )



if __name__ == '__main__':
    with open(SECRET_JSON, 'r') as f:
        secret = json.load(f)
    updater = Updater(secret['bot_token'])

    updater.dispatcher.add_handler(CommandHandler('start', start_command))
    updater.dispatcher.add_handler(CommandHandler('help', help_command))
    updater.dispatcher.add_handler(CommandHandler('list', list_command))
    updater.dispatcher.add_handler(CommandHandler('wol', wol_command,
                                                  pass_args=True,
                                                  pass_job_queue=True))

    updater.start_polling()
    updater.idle()
